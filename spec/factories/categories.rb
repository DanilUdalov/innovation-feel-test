FactoryBot.define do
  factory :category do
    sequence(:name) { |n| "#{Faker::Job.field} #{n}" }
  end
end
