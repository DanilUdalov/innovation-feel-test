FactoryBot.define do
  factory :user do
    first_name Faker::Name.first_name
    surname Faker::Name.last_name
    username Faker::Internet.user_name
    sequence(:email) { Faker::Internet.email }
    birthday Faker::Date.birthday(10, 60)
    password 'secret_password'
    password_confirmation 'secret_password'
    auth_token SecureRandom.urlsafe_base64
    auth_token_expires_at Time.now + 7.days
  end
end
