FactoryBot.define do
  factory :comment do
    body Faker::HarryPotter.quote
    user
    article
  end
end
