FactoryBot.define do
  factory :article do
    title Faker::Science.element
    body Faker::Lorem.sentences
    category
    user

    trait :with_images do
      after(:create) do |article|
        create_list(:image, 5, article: article)
      end
    end
  end
end
