FactoryBot.define do
  factory :image do
    picture ActionDispatch::Http::UploadedFile.new(tempfile: File.new("#{Rails.root}/spec/fixtures/images/test.jpg"), filename: 'test.jpg')
    article
  end
end
