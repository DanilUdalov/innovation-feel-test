require 'rails_helper'

RSpec.describe V1::ArticlesAPI, type: :request do
  include_context 'api_auth'
  let(:category) { create(:category) }

  before { subject }

  describe 'POST /api/articles' do
    let(:article) { Article.last }

    subject { post '/api/articles', params: params, headers: headers }

    context 'when article params are valid' do
      include_context 'articles_params'

      it { expect(response.status).to eq 201 }
      it { expect(response.body).to include article.title && article.body }
    end

    context 'when article params are not valid' do
      let(:params) { { auth_token: token, article: { title: '', body: '', category_id: '' } } }

      it { expect(response.status).to eq 400 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  describe 'GET /api/articles' do
    subject { get '/api/articles', headers: headers }

    let!(:posted_articles) { create_list(:article, 3, status: :posted) }
    let!(:article) { create(:article) }
    let!(:articles) { Article.only_posted }

    context 'show only posted articles' do
      it { expect(response.status).to eq 200 }
      it { expect(articles.count).to eq 3 }
    end
  end

  let(:article) { create(:article, user: user) }

  describe 'GET /api/articles/{id}' do
    subject { get "/api/articles/#{article.id}", headers: headers }

    context 'when article is present' do
      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include article.title }
    end

    context 'when article is not present' do
      subject { get '/api/articles/0', headers: headers }

      it { expect(response.status).to eq 404 }
    end
  end

  describe 'GET /api/articles/{id}/comments' do
    subject { get "/api/articles/#{article.id}/comments", headers: headers }

    let!(:comments) { create_list(:comment, 3, article: article) }

    context 'when article comments are present' do
      it { expect(response.status).to eq 200 }
      it { expect(comments.count).to eq 3 }
    end
  end

  describe 'PUT /api/articles/{id}' do
    subject { put "/api/articles/#{article.id}", params: params, headers: headers }

    context 'when user updates an article' do
      include_context 'articles_params'

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include article.reload.body }
    end
  end

  include_context 'api_admin_auth'

  describe 'GET /api/articles/export_all' do
    subject { get '/api/articles/export_all', headers: headers }

    let!(:articles) { build_list(:article, 15) }

    context 'export all articles' do
      it { expect(response.status).to eq 200 }
      it { expect(articles.count).to eq 15 }
    end
  end

  describe 'PUT /api/articles/{id}/update_status' do
    subject { put "/api/articles/#{article.id}/update_status", params: params, headers: headers }

    context 'when user updates an article' do
      let(:params) { { status: 'posted' } }

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include article.reload.status }
    end
  end

  describe 'DELETE /api/articles/{id}' do
    context 'when params are valid' do
      subject { delete "/api/articles/#{article.id}", headers: headers }

      it { expect(response.status).to eq 204 }
      it { expect(Article.exists?(article.id)).to be_falsey }
    end

    context 'when params are not valid' do
      subject { delete '/api/articles/0', headers: headers }

      it { expect(Article.exists?(article.id)).to be_truthy }
    end
  end
end
