require 'rails_helper'

RSpec.describe V1::CategoriesAPI, type: :request do
  include_context 'api_admin_auth'

  before { subject }

  describe 'GET /api/categories/category_select' do
    subject { get '/api/categories/category_select', headers: headers }

    let!(:categories) { create_list(:category, 3) }

    context 'render all categories' do
      it { expect(response.status).to eq 200 }
      it { expect(categories.count).to eq 3 }
    end
  end

  describe 'GET /api/categories/export_all' do
    subject { get '/api/categories/export_all', headers: headers }

    let!(:categories) { build_list(:category, 12) }

    context 'export all articles' do
      it { expect(response.status).to eq 200 }
      it { expect(categories.count).to eq 12 }
    end
  end

  describe 'POST /api/categories' do
    let(:category) { Category.last }

    subject { post '/api/categories', params: params, headers: headers }

    context 'when category params are valid' do
      let(:params) { { category: { name: Faker::Job.field } } }

      it { expect(response.status).to eq 201 }
      it { expect(response.body).to include category.name }
    end

    context 'when category params are not valid' do
      let(:params) { { category: { name: '' } } }

      it { expect(response.status).to eq 400 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  let(:category) { create(:category) }

  describe 'GET /api/categories/{id}' do
    subject { get "/api/categories/#{category.id}", headers: headers }

    context 'when category is present' do
      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include category.name }
    end

    context 'when category is not present' do
      subject { get '/api/categories/0', headers: headers }

      it { expect(response.status).to eq 404 }
    end
  end

  describe 'PUT /api/categories/{id}' do
    subject { put "/api/categories/#{category.id}", params: params, headers: headers }

    context 'when user updates a category' do
      let(:params) { { category: { name: Faker::Job.field } } }

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include category.reload.name }
    end

    context 'when user can not update a category' do
      let(:params) { { category: { name: '' } } }

      it { expect(response.status).to eq 422 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  describe 'DELETE /api/categories/{id}' do
    context 'when params are valid' do
      subject { delete "/api/categories/#{category.id}", headers: headers }

      it { expect(response.status).to eq 204 }
      it { expect(Category.exists?(category.id)).to be_falsey }
    end

    context 'when params are not valid' do
      subject { delete '/api/categories/0', headers: headers }

      it { expect(Category.exists?(category.id)).to be_truthy }
    end
  end
end
