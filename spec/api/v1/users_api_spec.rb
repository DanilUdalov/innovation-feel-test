require 'rails_helper'

RSpec.describe V1::UsersAPI, type: :request do
  include_context 'api_admin_auth'

  before { subject }

  describe 'GET /api/users' do
    subject { get '/api/users', headers: headers }

    let!(:users_list) { create_list(:user, 5) }

    context 'show all users' do
      it { expect(response.status).to eq 200 }
      it { expect(users_list.count).to eq 5 }
    end
  end

  describe 'GET /api/users/export_all' do
    subject { get '/api/users/export_all', headers: headers }

    let!(:users) { build_list(:user, 11) }

    context 'export all users' do
      it { expect(response.status).to eq 200 }
      it { expect(users.count).to eq 11 }
    end
  end

  let(:target_user) { create(:user) }

  describe 'GET /api/users/{id}' do
    subject { get "/api/users/#{target_user.id}", headers: headers }

    context 'when user is present' do
      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include target_user.email }
    end

    context 'when user is not present' do
      subject { get '/api/users/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(response.body).to include 'Not found!' }
    end
  end

  describe 'PUT /api/users/{id}' do
    subject { put "/api/users/#{target_user.id}", params: params, headers: headers }

    context 'when users params is valid' do
      let(:params) do
        { user: {
          email:                  'testing@gmail.com',
          password:               'my_password',
          password_confirmation:  'my_password'
        } }
      end

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include target_user.reload.email }
    end

    context 'when users params is not valid' do
      let(:params) do
        { user: {
          email:                  '',
          password:               'qaz',
          password_confirmation:  'qaz'
        } }
      end

      it { expect(response.status).to eq 422 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  describe 'PUT /api/users/{id}/update_role' do
    subject { put "/api/users/#{target_user.id}/update_role", params: params, headers: headers }

    context 'when admin updates an user role' do
      let(:params) { { role: 'moderator' } }

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include target_user.reload.role }
    end

    context 'when user does not able to change user role' do
      include_context 'api_auth'

      let(:params) { { role: 'admin' } }

      it { expect(response.status).to eq 401 }
      it { expect(response.body).to include 'Unauthorized query!' }
    end
  end

  describe 'DELETE /api/users/{id}' do
    context 'when user are valid' do
      subject { delete "/api/users/#{target_user.id}", headers: headers }

      it { expect(response.status).to eq 204 }
      it { expect(User.exists?(target_user.id)).to be_falsey }
    end

    context 'when user are not valid' do
      subject { delete '/api/users/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(User.exists?(target_user.id)).to be_truthy }
    end
  end
end
