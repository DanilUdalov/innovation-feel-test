require 'rails_helper'

RSpec.describe V1::ImagesAPI, type: :request do
  include_context 'api_auth'

  let(:article) { create(:article, user: user) }
  let(:image) { create(:image, article: article) }

  before { subject }

  describe 'GET /api/images' do
    subject { get "/api/images/#{image.id}", headers: headers }

    context 'when image is present' do
      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include image.picture.thumb.url }
    end

    context 'when image is not present' do
      subject { get '/api/images/hello_world', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(response.body).to include 'Not found!' }
    end
  end

  describe 'POST /api/images' do
    subject { post '/api/images', params: params, headers: headers }

    context 'when image params are valid' do
      include_context 'images_params'

      it { expect(response.status).to eq 201 }
      it { expect(response.body).to include Image.last.picture.thumb.url }
    end
  end

  describe 'PUT /api/images' do
    subject { put "/api/images/#{image.id}/make_front", headers: headers }

    context 'when image is valid' do
      it { expect(response.status).to eq 200 }
      it { expect(image.reload.front).to be_truthy }
    end

    context 'when image is not valid' do
      subject { get '/api/images/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(response.body).to include 'Not found!' }
    end
  end

  describe 'DELETE /api/images' do
    context 'when params are valid' do
      subject { delete "/api/images/#{image.id}", headers: headers }

      it { expect(response.status).to eq 200 }
      it { expect(Image.exists?(image.id)).to be_falsey }
    end

    context 'when params are not valid' do
      subject { delete '/api/images/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(Image.exists?(image.id)).to be_truthy }
    end
  end
end
