require 'rails_helper'

RSpec.describe V1::CommentsAPI, type: :request do
  include_context 'api_auth'

  let!(:article) { create(:article) }
  let!(:comment) { create(:comment, user: user, article: article) }

  before { subject }

  describe 'GET /api/comments' do
    subject { get '/api/comments', headers: headers }

    let!(:comments) { create_list(:comment, 5, article: article) }

    context 'show only posted articles' do
      it { expect(response.status).to eq 200 }
      it { expect(comments.count).to eq 5 }
    end
  end

  describe 'POST /api/comments' do
    subject { post '/api/comments', params: params, headers: headers }

    context 'when comment params are valid' do
      include_context 'comments_params'

      it { expect(response.status).to eq 201 }
      it { expect(response.body).to include Comment.last.body }
    end

    context 'when comment params are not valid' do
      let(:params) { { comment: { body: '', article_id: '' } } }

      it { expect(response.status).to eq 400 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  describe 'GET /api/comments/{id}' do
    subject { get "/api/comments/#{comment.id}", headers: headers }

    context 'when comment is present' do
      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include comment.body }
    end

    context 'when comment is not present' do
      subject { get '/api/comments/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(response.body).to include 'Not found!' }
    end
  end

  describe 'PUT /api/comments/{id}' do
    subject { put "/api/comments/#{comment.id}", params: params, headers: headers  }

    context 'when comment params is valid' do
      include_context 'comments_params'

      it { expect(response.status).to eq 200 }
      it { expect(response.body).to include comment.reload.body }
    end

    context 'when comment params is not valid' do
      let(:params) do
        {
          comment: {
            body: '',
            article_id: article.id
          }
        }
      end

      it { expect(response.status).to eq 422 }
      it { expect(response.body).to include "can't be blank" }
    end
  end

  describe 'DELETE /api/comments/{id}' do
    context 'when params are valid' do
      subject { delete "/api/comments/#{comment.id}", headers: headers }

      it { expect(response.status).to eq 204 }
      it { expect(Comment.exists?(comment.id)).to be_falsey }
    end

    context 'when params are not valid' do
      subject { delete '/api/comments/0', headers: headers }

      it { expect(response.status).to eq 404 }
      it { expect(Comment.exists?(comment.id)).to be_truthy }
    end
  end

  include_context 'api_admin_auth'

  describe 'GET /api/comments/export_all' do
    subject { get '/api/comments/export_all', headers: headers }

    let!(:comments) { build_list(:comment, 14) }

    context 'export all articles' do
      it { expect(response.status).to eq 200 }
      it { expect(comments.count).to eq 14 }
    end
  end
end
