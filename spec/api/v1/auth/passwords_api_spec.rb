require 'rails_helper'

RSpec.describe V1::Auth::PasswordsAPI, type: :request do
  include_context 'api_auth'

  before { subject }

  describe 'PUT /api/forgot_password' do
    subject { put '/api/passwords/forgot_password', params: params, headers: headers }

    context 'when user forgot password' do
      let(:params) do
        {
          email: user.email
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 200
      end

      it 'expects forgot password' do
        expect(response.body).to include user.reload.password_reset_token
      end
    end

    context 'when user did not enter email' do
      let(:params) { { email: '' } }

      it 'returns error status' do
        expect(response.status).to eq 404
      end
    end
  end

  describe 'PUT /api/forgot_password' do
    let(:user) { create(:user, password_reset_token: SecureRandom.urlsafe_base64, password_reset_sent_at: Time.zone.now) }

    subject { put "/api/passwords/#{user.password_reset_token}/set_new_password", params: params, headers: headers }

    context 'when user sets new password' do
      let(:params) do
        {
          password_reset_token:    user.password_reset_token,
          password:               'my_password',
          password_confirmation:  'my_password'
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 200
      end

      it 'expects new password' do
        expect(response.body).to include 'Your password already changed!'
      end
    end

    context 'when user data is not correct' do
      let(:params) do
        {
          password_reset_token:     user.password_reset_token,
          password:                 'my_password',
          password_confirmation:    'not_my_password'
        }
      end

      it 'returns error status' do
        expect(response.status).to eq 400
      end

      it 'expects error message' do
        expect(response.body).to include "doesn't match Password"
      end
    end
  end
end
