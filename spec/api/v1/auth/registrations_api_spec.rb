require 'rails_helper'

RSpec.describe V1::Auth::RegistrationsAPI, type: :request do
  include_context 'api_auth'

  before { subject }

  describe 'POST /api/sign_up' do
    subject { post '/api/registrations/sign_up', params: params, headers: headers }

    context 'when user signed up' do
      let(:params) do
        {
          email:                  'testing@gmail.com',
          password:               'my_password',
          password_confirmation:  'my_password'
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 201
      end

      it 'expects sign up' do
        expect(response.body).to include 'testing@gmail.com'
      end
    end

    context 'when user entered no valid dates' do
      let(:params) do
        {
          email:  Faker::Internet.email,
          password:  '',
          password_confirmation: ''
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 401
      end

      it 'expects error message' do
        expect(response.body).to include "can't be blank"
      end
    end
  end
end
