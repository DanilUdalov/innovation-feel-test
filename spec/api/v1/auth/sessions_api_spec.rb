require 'rails_helper'

RSpec.describe V1::Auth::SessionsAPI, type: :request do
  include_context 'api_auth'

  before { subject }

  describe 'POST /api/log_in' do
    subject { post '/api/sessions/log_in', params: params, headers: headers }

    context 'when user with email is present' do
      let(:params) do
        {
          email:    user.email,
          password: user.password
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 201
      end

      it 'expects log in' do
        expect(response.body).to include user.email
      end
    end

    context 'when user with email is not present' do
      let(:params) do
        {
          email:    Faker::Internet.email,
          password: Faker::Internet.password
        }
      end

      it 'returns error status' do
        expect(response.status).to eq 401
      end

      it 'expects error message' do
        expect(response.body).to include 'Authentication failed'
      end
    end
  end

  let!(:user) { create(:user) }
  let(:token) { user.auth_token }

  describe 'DELETE /api/log_in' do
    let!(:headers) { { 'Accept-Version' => 'v1', 'Auth-Token' => token } }

    subject { delete '/api/sessions/log_out', headers: headers }

    context 'when user is loging out' do
      let(:params) { { auth_token: token } }

      it 'returns success status' do
        expect(response.status).to eq 200
      end

      it 'expects log out' do
        expect(response.body).to include 'You logged out!'
      end
    end

    context 'when user entered no valid token' do
      let(:token) { SecureRandom.urlsafe_base64 }

      it 'returns error status' do
        expect(response.status).to eq 404
      end

      it 'expects error message' do
        expect(response.body).to include 'Not found!'
      end
    end
  end
end
