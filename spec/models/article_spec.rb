require 'rails_helper'

RSpec.describe Article, type: :model do
  describe 'associations' do
    %i[category user].each { |model| it { is_expected.to belong_to(model) } }

    %i[comments images].each { |model| it { is_expected.to have_many(model).dependent(:destroy) } }
  end

  describe 'enum' do
    it { is_expected.to define_enum_for(:status).with(pending: 0, posted: 1, blocked: 2) }
  end

  describe 'validates' do
    %i[body title].each { |model| it { is_expected.to validate_presence_of(model) } }
  end

  describe '#only_posted' do
    let!(:posted_articles) { create_list(:article, 2, status: :posted) }
    let!(:pending_articles) { create_list(:article, 3, status: :pending) }

    it 'expectes only posted articles' do
      expect(Article.only_posted).to eq posted_articles
    end
  end
end
