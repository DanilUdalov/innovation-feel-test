require 'rails_helper'

RSpec.describe Image, type: :model do
  let!(:picture) { ActionDispatch::Http::UploadedFile.new(tempfile: File.new("#{Rails.root}/spec/fixtures/images/test.jpg"), filename: 'test.jpg') }

  describe 'associations' do
    it { is_expected.to belong_to(:article) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:picture) }
    it { validate_length_of(:picture).is_at_most(5.megabytes) }
  end

  describe 'article must not have more than 5 images' do
    let(:article) { create(:article, :with_images) }

    it 'expects validation error' do
      image = article.images.create(picture: picture, article_id: article.id)

      expect(image.errors[:image]).to eq ['The article must not exceed more than 5 images']
    end
  end
end
