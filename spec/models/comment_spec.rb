require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'associations' do
    %i[article user].each { |model| it { is_expected.to belong_to(model) } }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:body) }
  end
end
