require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'associations' do
    %i[articles comments].each { |model| it { is_expected.to have_many(model).dependent(:destroy) } }
  end

  describe 'enum' do
    it { is_expected.to define_enum_for(:role).with(writter: 0, moderator: 1, admin: 2) }
  end

  describe 'validates' do
    it { have_secure_token }
    it { have_secure_password }

    %i[email password].each { |model| it { is_expected.to validate_presence_of(model) } }

    it { validate_confirmation_of(:password) }
    it { validate_length_of(:password).is_at_least(6) }
    it { is_expected.to validate_uniqueness_of(:email) }
  end

  describe '#full_name' do
    let(:user) { build(:user) }

    it 'expects full name' do
      expect(user.full_name).to eq("#{user.first_name} #{user.surname}")
    end
  end

  describe '#auth_token_expiration' do
    let(:user) { build(:user, auth_token: SecureRandom.urlsafe_base64, auth_token_expires_at: DateTime.now + 7) }

    it 'is not expired auth token' do
      expect(user.auth_token_expired?).to be_falsey
    end
  end
end
