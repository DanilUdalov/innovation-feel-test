require 'rails_helper'

RSpec.describe UserPolicy do
  subject { UserPolicy.new(user, record) }

  let(:record) { create(:user) }

  describe 'being a visitor' do
    context 'visitior permission' do
      let(:user) { nil }

      it { is_expected.to permit_action(:show) }
      it { is_expected.to forbid_actions(%i[export_all edit update]) }
    end
  end

  describe 'being a writter' do
    context 'when writter manages his profile' do
      let(:user) { record }

      it { is_expected.to permit_actions(%i[show edit update]) }
    end

    context 'when writter can`t change another`s profile' do
      let(:user) { create(:user) }

      it { is_expected.to permit_action([:show]) }
      it { is_expected.to forbid_actions(%i[export_all edit update destroy]) }
    end
  end

  describe 'being a moderator' do
    let(:user) { create(:user, role: :moderator) }

    context 'when moderator can`t change another profile' do
      it { is_expected.to permit_action([:show]) }
      it { is_expected.to forbid_actions(%i[export_all edit update]) }
    end

    context 'when moderator can change his profile' do
      let(:record) { user }

      it { is_expected.to permit_actions(%i[edit update]) }
    end
  end

  describe 'being an administrator' do
    let(:user) { create(:user, role: :admin) }

    context 'administrator permission' do
      it { is_expected.to permit_actions(%i[export_all show edit update destroy change_role]) }
    end
  end
end
