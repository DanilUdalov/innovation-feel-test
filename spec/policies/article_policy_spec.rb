require 'rails_helper'

RSpec.describe ArticlePolicy do
  subject { ArticlePolicy.new(user, article) }

  let(:article) { create(:article, status: :posted) }

  let(:scope) { ArticlePolicy::Scope.new(user, Article.all).resolve }

  describe 'being a visitor' do
    context 'visitior permission' do
      let(:user) { nil }

      it { is_expected.to permit_actions(%i[show index]) }
      it { is_expected.to forbid_actions(%i[export_all create edit update destroy]) }
    end
  end

  describe 'being a writter' do
    let(:user) { create(:user) }

    context 'when writter creates and manages his articles' do
      let(:article) { create(:article, user: user) }

      it { is_expected.to permit_actions(%i[show create edit update destroy]) }
      it { is_expected.to forbid_actions(%i[export_all change_status]) }
    end

    context 'when writter is trying change another`s article' do
      let(:article) { create(:article) }

      it { is_expected.to permit_actions(%i[index]) }
      it { is_expected.to forbid_actions(%i[export_all show edit update destroy change_status]) }
    end

    context 'accessing a posted article if user is writter' do
      let(:article) { create(:article, status: :posted) }

      it { expect(scope).to include(article) }
    end

    context 'accessing a pending article if user is writter' do
      let(:article) { create(:article) }

      it { expect(scope).not_to include(article) }
    end
  end

  describe 'being a moderator' do
    let(:user) { create(:user, role: :moderator) }

    context 'moderator permission' do
      it { is_expected.to permit_actions(%i[index show edit update change_status]) }
      it { is_expected.to forbid_actions(%i[export_all create destroy]) }
    end
  end

  describe 'being an administrator' do
    let(:user) { create(:user, role: :admin) }

    context 'administrator permission' do
      it { is_expected.to permit_actions(%i[export_all index create show edit update destroy change_status]) }
    end

    context 'accessing a pending article if user is admin' do
      let(:article) { create(:article) }

      it { expect(scope).to include(article) }
    end
  end
end
