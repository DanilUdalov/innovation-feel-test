require 'rails_helper'

RSpec.describe CommentPolicy do
  subject { CommentPolicy.new(user, comment) }

  let(:comment) { create(:comment) }

  describe 'being a visitor' do
    context 'visitior permission' do
      let(:user) { nil }

      it { is_expected.to permit_action(:show) }
      it { is_expected.to forbid_actions(%i[export_all create edit update destroy]) }
    end
  end

  describe 'being a writter' do
    let(:user) { create(:user) }

    context 'when writter creates comment' do
      let(:comment) { create(:comment, user: user) }

      it { is_expected.to permit_actions(%i[show create edit update destroy]) }
      it { is_expected.to forbid_action(:export_all) }
    end

    context 'when writter can`t change another`s comment' do
      let(:comment) { create(:comment) }

      it { is_expected.to permit_action([:show]) }
      it { is_expected.to forbid_actions(%i[export_all edit update destroy]) }
    end
  end

  describe 'being a moderator' do
    let(:user) { create(:user, role: :moderator) }

    context 'moderator permission' do
      it { is_expected.to permit_actions(%i[create show]) }
      it { is_expected.to forbid_actions(%i[export_all edit update destroy]) }
    end
  end

  describe 'being an administrator' do
    let(:user) { create(:user, role: :admin) }

    context 'administrator permission' do
      it { is_expected.to permit_actions(%i[export_all create show edit update destroy]) }
    end
  end
end
