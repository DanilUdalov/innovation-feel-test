require 'rails_helper'

RSpec.describe ImagePolicy do
  subject { ImagePolicy.new(user, image) }

  let(:image) { create(:image) }

  describe 'being a visitor' do
    context 'visitior permission' do
      let(:user) { nil }

      it { is_expected.to permit_action(:show) }
      it { is_expected.to forbid_actions(%i[create make_front destroy]) }
    end
  end

  describe 'being a writter' do
    let(:user) { create(:user) }

    context 'when writter attach image into the article' do
      let(:article) { create(:article, user: user) }
      let(:image) { create(:image, article: article) }

      it { is_expected.to permit_actions(%i[show create make_front destroy]) }
    end

    context 'when writter can`t attach image into another`s article' do
      let(:article) { create(:article) }
      let(:image) { create(:image, article: article) }

      it { is_expected.to permit_action([:show]) }
      it { is_expected.to forbid_actions(%i[make_front destroy]) }
    end
  end

  %i[moderator admin].each do |role|
    let(:user) { create(:user, role: role) }

    context "#{role} permission" do
      it { is_expected.to permit_actions(%i[create show make_front destroy]) }
    end
  end
end
