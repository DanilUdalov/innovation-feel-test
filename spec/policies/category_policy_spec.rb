require 'rails_helper'

RSpec.describe CategoryPolicy do
  subject { CategoryPolicy.new(user, category) }

  let(:category) { create(:category) }

  describe 'being a moderator' do
    let(:user) { create(:user, role: :moderator) }

    context 'moderator permission' do
      it { is_expected.to permit_action([:show]) }
      it { is_expected.to forbid_actions(%i[export_all edit update destroy]) }
    end
  end

  describe 'being an administrator' do
    let(:user) { create(:user, role: :admin) }

    context 'administrator permission' do
      it { is_expected.to permit_actions(%i[export_all create show edit update destroy]) }
    end
  end
end
