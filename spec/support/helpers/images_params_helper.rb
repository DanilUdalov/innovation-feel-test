shared_context 'images_params' do
  let(:params) do
    {
      image: {
        picture: fixture_file_upload("#{Rails.root}/spec/fixtures/images/test.jpg"),
        article_id: article.id
      }
    }
  end
end
