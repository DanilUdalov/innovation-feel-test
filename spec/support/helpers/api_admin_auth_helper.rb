shared_context 'api_admin_auth' do
  let!(:user) { create(:user, role: :admin) }
  let(:token) { user.auth_token }

  let!(:headers) { { 'Accept-Version' => 'v1', 'Auth-Token' => token } }
end
