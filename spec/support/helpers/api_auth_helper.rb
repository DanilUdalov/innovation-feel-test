shared_context 'api_auth' do
  let!(:user) { create(:user) }
  let(:token) { user.auth_token }

  let!(:headers) { { 'Accept-Version' => 'v1', 'Auth-Token' => token } }
end
