shared_context 'articles_params' do
  let(:params) do
    {
      article: {
        title: Faker::Science.element,
        body: Faker::Matz.quote,
        category_id: category.id
      }
    }
  end
end
