shared_context 'comments_params' do
  let(:params) do
    {
      comment: {
        body: Faker::FamilyGuy.quote,
        article_id: article.id
      }
    }
  end
end
