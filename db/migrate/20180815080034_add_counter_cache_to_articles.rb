class AddCounterCacheToArticles < ActiveRecord::Migration[5.1]
  def up
    add_column :articles, :comments_count, :integer, default: 0
 
    Article.reset_column_information
    Article.all.each { |article| Article.update_counters article.id, comments_count: article.comments.count }
  end
end
