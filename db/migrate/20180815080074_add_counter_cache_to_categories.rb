class AddCounterCacheToCategories < ActiveRecord::Migration[5.1]
  def up
    add_column :categories, :articles_count, :integer, default: 0
 
    Category.reset_column_information
    Category.all.each {|category| Category.update_counters category.id, articles_count: category.articles.count }
  end
end
