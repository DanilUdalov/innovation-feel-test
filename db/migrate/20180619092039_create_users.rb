class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :surname
      t.string :username
      t.string :email
      t.string :birthday
      t.string :password_digest
      t.integer :role, default: 0

      t.timestamps
    end
  end
end
