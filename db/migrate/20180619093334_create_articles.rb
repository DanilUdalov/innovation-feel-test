class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body
      t.belongs_to :category, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
