15.times do
  User.create(first_name: Faker::Name.first_name,
              surname: Faker::Name.last_name,
              username: Faker::Internet.user_name,
              email: Faker::Internet.email,
              birthday: Faker::Date.birthday(10, 60),
              password: 'secret_password',
              password_confirmation: 'secret_password')
end
10.times { Category.create(name: Faker::Job.field) }
25.times { Article.create(title: Faker::Science.element, body: Faker::Lorem.sentences, category: Category.last, user: User.last) }
