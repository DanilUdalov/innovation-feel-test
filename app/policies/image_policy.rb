class ImagePolicy < ApplicationPolicy
  permit_user_to :create, :make_front, :destroy, :is_admin_or_moderator_or_owner

  def permitted_attributes
    %i[picture front article_id]
  end

  def create?
    user.present?
  end

  def owner?
    record.article.user == user
  end
end
