class ArticlePolicy < ApplicationPolicy
  permit_user_to :destroy, :user_is_admin_or_owner
  permit_user_to :update, :is_admin_or_moderator_or_owner

  def permitted_attributes
    %i[title body category_id user_id]
  end

  def index?
    true
  end

  def create?
    user.present? && (user.writter? || user.admin?)
  end

  def show?
    owner? || record.posted? || user.try(:admin?) || user.try(:moderator?)
  end

  def change_status?
    user.present? && (user.admin? || user.moderator?)
  end

  class Scope < Scope
    def resolve
      if user.try(:admin?) || user.try(:moderator?)
        scope
      elsif user.try(:writter?)
        scope.only_posted.or(scope.where(user: user))
      else
        scope.only_posted
      end
    end
  end
end
