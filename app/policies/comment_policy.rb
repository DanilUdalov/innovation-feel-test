class CommentPolicy < ApplicationPolicy
  permit_user_to :update, :destroy, :user_is_admin_or_owner

  def permitted_attributes
    %i[body article_id user_id]
  end

  def create?
    user.present?
  end
end
