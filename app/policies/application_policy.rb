class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    false
  end

  def export_all?
    user.present? && user.admin?
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  def self.permit_user_to(*actions, some_method)
    actions.each { |action| define_method("#{action}?") { send(some_method) } }
  end

  def owner?
    record.user == user
  end

  def user_is_admin
    user.present? && user.admin?
  end

  def user_is_admin_or_owner
    user.present? && (user.admin? || owner?)
  end

  def is_admin_or_moderator_or_owner
    user.present? && (user.admin? || user.moderator? || owner?)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
