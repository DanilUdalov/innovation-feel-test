class CategoryPolicy < ApplicationPolicy
  permit_user_to :create, :update, :destroy, :user_is_admin

  def permitted_attributes
    %i[name]
  end
end
