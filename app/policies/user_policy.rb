class UserPolicy < ApplicationPolicy
  permit_user_to :destroy, :user_is_admin
  permit_user_to :update, :user_is_admin_or_owner

  def permitted_attributes
    %i[first_name surname email username birthday password_digest role]
  end

  def change_role?
    user.present? && user.admin?
  end

  def owner?
    record == user
  end
end
