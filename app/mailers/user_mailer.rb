class UserMailer < ApplicationMailer
  default from: ENV['SENDGRID_USERNAME']

  def password_resets(user)
    @user = user
    mail to: user.email, subject: I18n.t('mailer.passwords.subject.password_reseting')
  end
end
