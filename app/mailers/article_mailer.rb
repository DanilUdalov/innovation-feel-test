class ArticleMailer < ApplicationMailer
  default from: ENV['SENDGRID_USERNAME']

  before_action { @article = params[:article] }

  def change_status
    mail to: @article.user.email, subject: I18n.t('mailer.articles.subject.changed_status', id: @article.id, status: @article.status)
  end

  def destroy
    mail to: @article.user.email, subject: I18n.t('mailer.articles.subject.deleted_article', id: @article.id)
  end
end
