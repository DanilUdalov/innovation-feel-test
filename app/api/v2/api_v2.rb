class V2::ApiV2 < ApplicationAPI
  version %w[v2 v1], using: :accept_version_header

  mount V1::Auth::PasswordsAPI
  mount V1::Auth::RegistrationsAPI
  mount V1::Auth::SessionsAPI
  mount V1::ArticlesAPI
  mount V1::CommentsAPI
  mount V1::CategoriesAPI
  mount V1::UsersAPI
  mount V1::ImagesAPI
end
