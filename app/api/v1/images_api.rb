class V1::ImagesAPI < V1::ApiV1
  before { api_version_valid? }

  resource :images do
    helpers do
      params :images_params do
        requires :image, type: Hash do
          requires :picture, type: File, desc: I18n.t('inputs.image')
          requires :article_id, type: Integer, desc: I18n.t('inputs.article_id')
          optional :front, type: Boolean, desc: I18n.t('inputs.front_image')
        end
      end
    end

    desc I18n.t('descriptions.create_image'), headers(required_token: true, include_auth_token: true)
    params { use :images_params }
    post do
      authorize Image, :create?
      image = Image.create(params[:image])
      error!({ errors: image.errors }, 400) if image.errors.present?
      present image, with: V1::Entities::ImageEntity
    end

    route_param :id do
      before { @image = Image.find(params[:id]) }

      desc I18n.t('descriptions.show_image'), headers(required_token: false, include_auth_token: true)
      get do
        authorize @image, :show?
        present @image, with: V1::Entities::ImageEntity
      end

      desc I18n.t('descriptions.make_front'), headers(required_token: true, include_auth_token: true)
      put :make_front do
        authorize @image, :make_front?
        @image.article.images.update_all(front: false)
        @image.update(front: true)
        present @image.reload, with: V1::Entities::ImageEntity
      end

      desc I18n.t('descriptions.destroy_image'), headers(required_token: true, include_auth_token: true)
      delete do
        authorize @image, :destroy?
        @image.destroy!
        { message: I18n.t('messages.deleted_image') }
      end
    end
  end
end
