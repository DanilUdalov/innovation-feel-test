class V1::ArticlesAPI < V1::ApiV1
  before { api_version_valid? }

  resource :articles do
    helpers do
      def articles_params
        ActionController::Parameters.new(params[:article]).permit(:title, :body, :category_id)
      end

      params :articles_params do
        requires :article, type: Hash do
          requires :title,       type: String,  desc: I18n.t('inputs.title')
          requires :body,        type: String,  desc: I18n.t('inputs.body')
          requires :category_id, type: Integer, desc: I18n.t('inputs.category')
        end
      end
    end

    desc I18n.t('descriptions.show_only_posted_articles'), headers(required_token: false, include_auth_token: true)
    paginate per_page: 10
    params do
      optional :user_id, type: Integer, desc: I18n.t('inputs.user_id')
      optional :sort_by, type: Symbol, values: %i[id title status category user comments_count created_at]
      optional :order, type: Symbol, values: %i[asc desc]
    end
    get do
      articles = paginate policy_scope(Article.sorting(params[:sort_by], params[:order]))
      articles = articles.where(user_id: params[:user_id]) if params[:user_id].present?
      filling_headers(articles)
      V1::Entities::ArticleEntity.represent articles
    end

    desc I18n.t('descriptions.export_all_articles'), headers(required_token: true, include_auth_token: true)
    get :export_all do
      authorize Article, :export_all?
      articles = policy_scope(Article.order(:id))
      present articles, with: V1::Entities::ArticleEntity
    end

    desc I18n.t('descriptions.create_an_article'), headers(required_token: true, include_auth_token: true)
    params { use :articles_params }
    post do
      authorize Article, :create?
      article = current_user.articles.create(articles_params)
      error!({ errors: article.errors }, 400) unless article.persisted?
      present article, with: V1::Entities::ArticleEntity
    end

    route_param :id do
      before { @article = Article.find(params[:id]) }

      desc I18n.t('descriptions.show_an_article'), headers(required_token: false, include_auth_token: true)
      get do
        authorize @article, :show?
        present @article, with: V1::Entities::ArticleEntity
      end

      desc I18n.t('descriptions.show_article_comments'), headers(required_token: false, include_auth_token: true)
      get :comments do
        authorize @article, :show?
        present policy_scope(@article.comments.order(id: :desc)), with: V1::Entities::CommentEntity
      end

      desc I18n.t('descriptions.update_an_article'), headers(required_token: true, include_auth_token: true)
      params { use :articles_params }
      put do
        authorize @article, :update?
        if @article.update(articles_params)
          present @article.reload, with: V1::Entities::ArticleEntity
        else
          error!({ errors: @article.errors }, 422)
        end
      end

      desc I18n.t('descriptions.update_an_article_status'), headers(required_token: true, include_auth_token: true)
      params do
        requires :status, type: String, values: Article.statuses.keys
      end
      put :update_status do
        authorize @article, :change_status?
        @article.send("#{params[:status]}!")
        ArticleMailer.with(article: @article).change_status.deliver
        present @article.reload, with: V1::Entities::ArticleEntity
      end

      desc I18n.t('descriptions.destroy_an_article'), headers(required_token: true, include_auth_token: true)
      delete do
        authorize @article, :destroy?
        @article.destroy!
        ArticleMailer.with(article: @article).destroy.deliver
        status 204
      end
    end
  end
end
