class V1::Auth::RegistrationsAPI < V1::ApiV1
  before { api_version_valid? }

  resource :registrations do
    desc I18n.t('descriptions.sign_up_user'), headers(required_token: false, include_auth_token: false)
    params do
      optional :first_name,            type: String, desc: I18n.t('inputs.first_name')
      optional :surname,               type: String, desc: I18n.t('inputs.surname')
      optional :username,              type: String, desc: I18n.t('inputs.username')
      optional :birthday,              type: Date,   desc: I18n.t('inputs.birthday')
      requires :email,                 type: String, desc: I18n.t('inputs.email')
      requires :password,              type: String, desc: I18n.t('inputs.password')
      requires :password_confirmation, type: String, desc: I18n.t('inputs.password_confirmation')
    end
    post :sign_up do
      if User.find_by_email(params[:email]).present?
        error!(I18n.t('errors.email_is_present'), 401)
      end
      User.transaction do
        user = User.new(params)
        if user.save
          user.set_auth_token
          session[:auth_token] = user.auth_token
          present user, with: V1::Entities::UserEntity
        else
          error!(user.errors, 401)
        end
      end
    end
  end
end
