class V1::Auth::SessionsAPI < V1::ApiV1
  before { api_version_valid? }

  resource :sessions do
    desc I18n.t('description.log_in_user'), headers(required_token: false, include_auth_token: false)
    params do
      requires :email,       type: String,  desc: I18n.t('inputs.email')
      requires :password,    type: String,  desc: I18n.t('inputs.password')
      optional :remember_me, type: Boolean, desc: I18n.t('inputs.remember_me')
    end
    post :log_in do
      user = User.find_by_email(params[:email])
      if user&.authenticate(params[:password])
        user.set_auth_token
        session[:auth_token] = user.auth_token
        cookies[:auth_token] = session[:auth_token] if params[:remember_me] == true
        present user, with: V1::Entities::UserEntity
      else
        error!(I18n.t('errors.auth_fail'), 401)
      end
    end

    desc I18n.t('description.log_out_user'), headers(required_token: true, include_auth_token: true)
    delete :log_out do
      if current_user.present?
        current_user.reset_auth_token
        session[:auth_token] = nil
        cookies.delete :auth_token
        { message: I18n.t('messages.you_logged_out') }
      else
        error!(I18n.t('errors.invalid_access_token'), 401)
      end
    end
  end
end
