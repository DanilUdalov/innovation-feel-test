class V1::Auth::PasswordsAPI < V1::ApiV1
  before { api_version_valid? }

  resource :passwords do
    desc I18n.t('descriptions.reset_password'), headers(required_token: false, include_auth_token: false)
    params do
      requires :email, type: String, desc: I18n.t('inputs.email')
    end
    put :forgot_password do
      @user = User.find_by_email(params[:email])
      error!(I18n.t('errors.not_found'), 404) unless @user.present?
      @user&.password_reset!
      {
        message:        I18n.t('messages.password_reset'),
        password_token: @user.password_reset_token
      }
    end

    route_param :password_reset_token do
      desc I18n.t('descriptions.set_new_password'), headers(required_token: false, include_auth_token: false)
      params do
        requires :password,              type: String, desc: I18n.t('inputs.new_password')
        requires :password_confirmation, type: String, desc: I18n.t('inputs.new_password_confirmation')
      end
      put :set_new_password do
        @user = User.find_by_password_reset_token(params[:password_reset_token])
        if !@user.present?
          error!(I18n.t('errors.not_found'), 404)
        elsif @user.password_reset_token_expired?
          error!(I18n.t('errors.password_has_expired', password_reset_token: @user.password_reset_token), 400)
        elsif @user.update(params)
          @user.set_auth_token
          session[:auth_token] = @user.auth_token
          { message: I18n.t('messages.password_arleady_changed') }
        else
          error!(@user.errors, 400)
        end
      end
    end
  end
end
