class V1::CommentsAPI < V1::ApiV1
  before { api_version_valid? }

  resource :comments, param: :id do
    helpers do
      def comments_params
        ActionController::Parameters.new(params[:comment]).permit(:body, :article_id)
      end

      params :comments_params do
        requires :comment, type: Hash do
          requires :body, type: String, desc: I18n.t('inputs.body')
          requires :article_id, type: Integer, desc: I18n.t('inputs.article')
        end
      end
    end

    desc I18n.t('descriptions.export_all_comments'), headers(required_token: true, include_auth_token: true)
    get :export_all do
      authorize Comment, :export_all?
      comments = policy_scope(Comment.order(:id))
      present comments, with: V1::Entities::CommentEntity
    end

    desc I18n.t('descriptions.get_all_comments'), headers(required_token: true, include_auth_token: true)
    paginate per_page: 10
    params do
      optional :sort_by, type: Symbol, values: %i[id body article user created_at updated_at]
      optional :order, type: Symbol, values: %i[asc desc]
    end
    get do
      comments = paginate policy_scope(Comment.sorting(params[:sort_by], params[:order]))
      filling_headers(comments)
      V1::Entities::CommentEntity.represent comments
    end

    desc I18n.t('descriptions.create_comment'), headers(required_token: true, include_auth_token: true)
    params { use :comments_params }
    post do
      authorize Comment, :create?
      comment = current_user.comments.create(comments_params)
      error!({ errors: comment.errors }, 400) unless comment.persisted?
      present comment, with: V1::Entities::CommentEntity
    end

    route_param :id do
      before { @comment = Comment.find(params[:id]) }

      desc I18n.t('descriptions.show_comment'), headers(required_token: true, include_auth_token: true)
      get do
        authorize @comment, :show?
        present @comment, with: V1::Entities::CommentEntity
      end

      desc I18n.t('descriptions.update_comment'), headers(required_token: true, include_auth_token: true)
      params { use :comments_params }
      put do
        authorize @comment, :update?
        if @comment.update(comments_params)
          present @comment.reload, with: V1::Entities::CommentEntity
        else
          error!({ errors: @comment.errors }, 422)
        end
      end

      desc I18n.t('descriptions.destroy_comment'), headers(required_token: true, include_auth_token: true)
      delete do
        authorize @comment, :destroy?
        @comment.destroy!
        status 204
      end
    end
  end
end
