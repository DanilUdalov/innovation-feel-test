class V1::ApiV1 < ApplicationAPI
  version 'v1', using: :accept_version_header

  mount V1::Auth::PasswordsAPI
  mount V1::Auth::RegistrationsAPI
  mount V1::Auth::SessionsAPI
  mount V1::ArticlesAPI
  mount V1::CommentsAPI
  mount V1::CategoriesAPI
  mount V1::UsersAPI
  mount V1::ImagesAPI

  add_swagger_documentation(
    info: { title: I18n.t('info.title') },
    api_version: 'v1',
    hide_documentation_path: true
  )
end
