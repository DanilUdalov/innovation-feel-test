class V1::CategoriesAPI < V1::ApiV1
  before { api_version_valid? }

  resource :categories do
    helpers do
      params :categories_params do
        requires :category, type: Hash do
          requires :name, type: String, desc: I18n.t('inputs.name')
        end
      end
    end

    desc I18n.t('descriptions.select_categories'), headers(required_token: false, include_auth_token: false)
    get :category_select do
      present Category.all, with: V1::Entities::CategoryEntity
    end

    desc I18n.t('descriptions.get_all_categories'), headers(required_token: false, include_auth_token: true)
    paginate per_page: 10
    params do
      optional :sort_by, type: Symbol, values: %i[id name created_at updated_at articles_count]
      optional :order, type: Symbol, values: %i[asc desc]
    end
    get do
      categories = paginate policy_scope(Category.sorting(params[:sort_by], params[:order]))
      filling_headers(categories)
      V1::Entities::CategoryEntity.represent categories
    end

    desc I18n.t('descriptions.export_all_categories'), headers(required_token: true, include_auth_token: true)
    get :export_all do
      authorize Category, :export_all?
      categories = policy_scope(Category.order(:id))
      present categories, with: V1::Entities::CategoryEntity
    end

    desc I18n.t('descriptions.create_category'), headers(required_token: true, include_auth_token: true)
    params { use :categories_params }
    post do
      authorize Category, :create?
      category = Category.create(params[:category])
      error!({ errors: category.errors }, 400) unless category.persisted?
      present category, with: V1::Entities::CategoryEntity
    end

    route_param :id do
      before { @category = Category.find(params[:id]) }

      desc I18n.t('descriptions.show_category'), headers(required_token: true, include_auth_token: true)
      get do
        authorize @category, :show?
        present @category, with: V1::Entities::CategoryEntity
      end

      desc I18n.t('descriptions.update_category'), headers(required_token: true, include_auth_token: true)
      params { use :categories_params }
      put do
        authorize @category, :update?
        if @category.update(params[:category])
          present @category.reload, with: V1::Entities::CategoryEntity
        else
          error!({ errors: @category.errors }, 422)
        end
      end

      desc I18n.t('descriptions.destroy_category'), headers(required_token: true, include_auth_token: true)
      delete do
        authorize @category, :destroy?
        @category.destroy!
        status 204
      end
    end
  end
end
