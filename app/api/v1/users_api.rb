class V1::UsersAPI < V1::ApiV1
  before { api_version_valid? }

  resource :users do
    desc I18n.t('descriptions.get_all_users'), headers(required_token: true, include_auth_token: true)
    params do
      optional :sort_by, type: Symbol, values: %i[id first_name surname username birthday email role created_at updated_at]
      optional :order, type: Symbol, values: %i[asc desc]
    end
    paginate per_page: 10
    get do
      users = paginate policy_scope(User.sorting(params[:sort_by], params[:order]))
      filling_headers(users)
      V1::Entities::UserEntity.represent users
    end

    desc I18n.t('descriptions.export_all_users'), headers(required_token: true, include_auth_token: true)
    get :export_all do
      authorize User, :export_all?
      users = policy_scope(User.order(:id))
      present users, with: V1::Entities::UserEntity
    end

    route_param :id do
      before { @user = User.find(params[:id]) }

      desc I18n.t('descriptions.show_user'), headers(required_token: false, include_auth_token: true)
      get do
        authorize @user, :show?
        present @user, with: V1::Entities::UserEntity
      end

      desc I18n.t('descriptions.update_user'), headers(required_token: true, include_auth_token: true)
      params do
        optional :user, type: Hash do
          optional :first_name,            type: String, desc: I18n.t('inputs.first_name')
          optional :surname,               type: String, desc: I18n.t('inputs.surname')
          optional :username,              type: String, desc: I18n.t('inputs.username')
          optional :birthday,              type: Date,   desc: I18n.t('inputs.birthday')
          optional :email,                 type: String, desc: I18n.t('inputs.email')
          optional :password,              type: String, desc: I18n.t('inputs.password')
          optional :password_confirmation, type: String, desc: I18n.t('inputs.password_confirmation')
        end
      end
      put do
        authorize @user, :update?
        if @user.update(params[:user])
          present @user.reload, with: V1::Entities::UserEntity
        else
          error!({ errors: @user.errors }, 422)
        end
      end

      desc I18n.t('descriptions.change_user_role'), headers(required_token: true, include_auth_token: true)
      params do
        requires :role, type: String, values: User.roles.keys
      end
      put :update_role do
        authorize @user, :change_role?
        @user.send("#{params[:role]}!")
        present @user.reload, with: V1::Entities::UserEntity
      end

      desc I18n.t('descriptions.destroy_user'), headers(required_token: true, include_auth_token: true)
      delete do
        authorize @user, :destroy?
        @user.destroy!
        status 204
      end
    end
  end
end
