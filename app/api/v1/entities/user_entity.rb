class V1::Entities::UserEntity < Grape::Entity
  expose :id, :email, :first_name, :surname, :username, :birthday, :role, :auth_token, :auth_token_expires_at
  format_with :timestamp do |date|
    date.strftime('%d %B %Y, %H:%M')
  end
  expose :created_at, :updated_at, format_with: :timestamp
end
