class V1::Entities::ImageEntity < Grape::Entity
  expose :id, :picture, :front
  expose :article, with: V1::Entities::ArticleEntity
end
