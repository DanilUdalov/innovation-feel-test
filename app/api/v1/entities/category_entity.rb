class V1::Entities::CategoryEntity < Grape::Entity
  expose :id, :name
  format_with :timestamp do |date|
    date.strftime('%d %B %Y, %H:%M')
  end
  expose :articles_count
  expose :created_at, :updated_at, format_with: :timestamp
end
