class V1::Entities::CommentEntity < Grape::Entity
  expose :id, :body, :article, :article_id, :user
  format_with :timestamp do |date|
    date.strftime('%d %B %Y, %H:%M')
  end
  expose :created_at, :updated_at, format_with: :timestamp
end
