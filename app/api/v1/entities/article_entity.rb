class V1::Entities::ArticleEntity < Grape::Entity
  expose :id, :title, :body, :category_id
  expose :category, with: V1::Entities::CategoryEntity
  format_with :timestamp do |date|
    date.strftime('%d %B %Y, %H:%M')
  end
  expose :created_at, as: :posted_date, format_with: :timestamp
  expose :status
  expose :user
  expose :comments_count
  expose :front_image, :images_count
  expose :images do |article|
    article.images.order(front: :desc)
  end
end
