class ApplicationAPI < Grape::API
  use ActionDispatch::Session::CookieStore

  API_VERSIONS = %w[v1 v2]

  prefix 'api'
  rescue_from :all
  format :json

  rescue_from ActiveRecord::RecordNotFound do
    rack_response({ error: I18n.t('errors.not_found') }.to_json, 404)
  end

  rescue_from Pundit::NotAuthorizedError do
    rack_response({ error: I18n.t('errors.unauth_query') }.to_json, 401)
  end

  def self.headers(params)
    header_hash = {
      'Accept-Version' => { description: I18n.t('descriptions.api_version'), required: true, values: API_VERSIONS },
      'Auth-Token' => { description: I18n.t('inputs.auth_token'), required: params[:required_token] }
    }
    { headers: params[:include_auth_token] ? header_hash : header_hash.slice('Accept-Version') }
 end

  helpers Pundit

  helpers do
    def session
      env['rack.session']
    end

    def filling_headers(records)
      header['X-Total-Pages'] = records.total_pages
      end_point = header['X-Total-Pages'] - 1
      header['Content-Range'] = "bytes 0-#{end_point}/#{header['X-Total']}"
    end

    def current_user
      if headers['Auth-Token'].present?
        @current_user ||= User.find_by_auth_token(headers['Auth-Token'])
        error!(I18n.t('errors.not_found'), 404) unless @current_user.present?
        error!(I18n.t('errors.session_is_overdue'), 401) if @current_user.auth_token_expired?
        @current_user
      end
    end

    def api_version_valid?
      if headers['Accept-Version'].blank?
        error!({ error: I18n.t('errors.pass_api_version') }, 401)
      elsif !API_VERSIONS.include?(headers['Accept-Version'])
        error!({ error: I18n.t('errors.pass_only_correct_api') }, 401)
      end
    end
  end

  mount V1::ApiV1
  mount V2::ApiV2
end
