module Sortable
  extend ActiveSupport::Concern

  def sorting(column, order)
    order("#{column} #{order}")
  end
end
