class Article < ApplicationRecord
  extend Sortable

  enum status: %i[pending posted blocked]

  belongs_to :category, counter_cache: true
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :images, dependent: :destroy

  validates_presence_of :body
  validates_presence_of :title

  scope :only_posted, -> { where(status: :posted) }

  def self.sorting(column, order)
    if column == :category
      includes(:category).order("categories.name #{order}")
    elsif column == :user
      includes(:user).order("users.email #{order}")
    else
      super(column, order)
    end
  end

  def front_image
    images.find_by_front(true)
  end

  def images_count
    images.count
  end
end
