class Category < ApplicationRecord
  extend Sortable

  has_many :articles, dependent: :destroy

  validates :name, uniqueness: true, presence: true
end
