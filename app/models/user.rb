class User < ApplicationRecord
  extend Sortable

  has_secure_password

  enum role: %i[writter moderator admin]

  has_many :articles, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password, on: :create, length: { minimum: 6 }
  validates_confirmation_of :password

  def full_name
    "#{first_name} #{surname}"
  end

  def set_auth_token
    self.auth_token = SecureRandom.urlsafe_base64
    auth_token_expiration
    save!
  end

  def set_password_reset_token
    self.password_reset_token = SecureRandom.urlsafe_base64
    password_token_expiration
    save!
  end

  def reset_auth_token
    self.auth_token = nil
    self.auth_token_expires_at = nil
    save!
  end

  def password_reset!
    set_password_reset_token
    save!
    UserMailer.password_resets(self).deliver
  end

  def auth_token_expiration
    self.auth_token_expires_at = DateTime.now + 7
  end

  def password_token_expiration
    self.password_reset_sent_at = Time.zone.now
  end

  def auth_token_expired?
    auth_token_expires_at < 7.days.ago
  end

  def password_reset_token_expired?
    password_reset_sent_at < 2.hours.ago
  end
end
