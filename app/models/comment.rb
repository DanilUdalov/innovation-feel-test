class Comment < ApplicationRecord
  extend Sortable

  belongs_to :user
  belongs_to :article, counter_cache: true

  validates_presence_of :body

  def self.sorting(column, order)
    if column == :article
      includes(:article).order("articles.id #{order}")
    elsif column == :user
      includes(:user).order("users.email #{order}")
    else
      super(column, order)
    end
  end
end
