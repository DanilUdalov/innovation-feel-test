class Image < ApplicationRecord
  belongs_to :article

  mount_uploader :picture, ImageUploader

  validates_presence_of :picture
  validates_size_of :picture, maximum: 5.megabytes, message: I18n.t('errors.exceed_file_size')
  validate :validate_count_images, on: :create

  def validate_count_images
    errors.add(:image, I18n.t('errors.exceed_images_count')) if article.present? && article.images.count >= 5
  end
end
