require_relative 'boot'

require 'rails/all'
require 'sprockets/railtie'

Bundler.require(*Rails.groups)

module InnovationFeelTest
  class Application < Rails::Application
    config.load_defaults 5.1
    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]
    config.autoload_paths << "#{Rails.root}/app/api/entites/"
    config.session_store :cookie_store, expire_after: 7.days
    config.api_only = true
  end
end
