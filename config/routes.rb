Rails.application.routes.draw do
  mount ApplicationAPI => '/'
  mount GrapeSwaggerRails::Engine => '/api/docs/'

  root to: 'pages#landing'
end
