ApiPagination.configure do |config|
  config.total_header = 'X-Total'
  config.page_header = 'X-Current-Page'
  config.per_page_header = 'X-Per-Page'
end
