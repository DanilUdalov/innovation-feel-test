Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins ENV['APP_PATH']
    resource '*',
      headers: :any,
      methods: %i(get post put patch delete options head),
      expose: [
       'X-Total',
       'X-Total-Pages',
       'X-Page',
       'X-Per-Page',
       'X-Next-Page',
       'X-Prev-Page',
       'X-Current-Page',
       'X-Offset',
       'X-Rdb-Lang',
       'Content-Range'
      ]
  end
end
