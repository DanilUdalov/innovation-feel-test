source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'api-pagination'
gem 'aws-sdk-rails', '~> 2'
gem 'aws-sdk-s3', '~> 1', require: false
gem 'bootstrap-sass', '~> 3.3.6'
gem 'bcrypt-ruby', require: 'bcrypt'
gem 'carrierwave', github: 'carrierwaveuploader/carrierwave'
gem 'cloudinary'
gem 'coffee-rails', '~> 4.2'
gem 'enumerize'
gem 'faker', '>= 1.6.1'
gem 'figaro'
gem 'fog-aws'
gem 'grape'
gem 'grape-entity'
gem 'grape_on_rails_routes'
gem 'grape-swagger'
gem 'grape-swagger-rails'
gem 'jbuilder', '~> 2.5'
gem 'mini_magick'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.7'
gem 'pundit'
gem 'rack-cors'
gem 'rails', '~> 5.1.5'
gem 'rest-client'
gem 'sass-rails', '~> 5.0'
gem 'sendgrid-ruby'
gem 'swagger-docs'
gem 'swagger-ui_rails'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'will_paginate'

group :development, :test do
  gem 'airborne'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 2.13'
  gem 'factory_bot_rails'
  gem 'rspec-rails', '>= 3.1.0'
  gem 'pundit-matchers', '~> 1.6.0'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 3.1'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'pry'
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :production do
  gem 'rails_12factor'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
